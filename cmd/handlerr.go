package cmd

import (
	"net/http"
)

// ServerError .
func (app *App) ServerError(w http.ResponseWriter, err error, from string) {
	// trace := fmt.Sprintf("%s \n %s", err, debug.Stack())
	// app.LogError.Output(2, trace)
	app.LogError.Printf("%v -> from : %v\n", err.Error(), from)
	http.Error(w, "server error", http.StatusInternalServerError)
}

// ClientError .
func (app *App) ClientError(w http.ResponseWriter, err error, status int) {
	app.LogInfo.Printf("Client : %v", err.Error())
	http.Error(w, err.Error(), status)
}
