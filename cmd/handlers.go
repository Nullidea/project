package cmd

import (
	"encoding/json"
	"fmt"
	"io/ioutil"
	"net/http"
	"net/mail"
	"project/cmd/helpers"
	"project/pkg/models"

	"github.com/go-sql-driver/mysql"
	"golang.org/x/oauth2"
)

// HandleMainPage ...
func (app *App) HandleMainPage(w http.ResponseWriter, r *http.Request) {
	if r.Method == "GET" {
		id := app.Sessions.GetInt(r, "id")
		template := app.View["mainProfile"]
		userProfile, err := app.Db.GetUserProfile(id)
		if err != nil && err != models.ErrNoRecord {
			app.ServerError(w, err, "HandleSiginIn : GetUserProfile")
			return
		}
		user, err := app.Db.GetUser(id)
		if err != nil {
			if err == models.ErrNoRecord {
				app.ClientError(w, err, http.StatusBadRequest)
				return
			}
			app.ServerError(w, err, "HandleSignIn : GetUser")
			return
		}
		if userProfile != nil {
			data := struct {
				Username        string
				FullName        string
				Address         string
				TelephoneNumber string
				Email           string
			}{
				Username:        user.Username,
				FullName:        userProfile.FullName,
				Address:         userProfile.Address,
				TelephoneNumber: userProfile.TelephoneNumber,
				Email:           userProfile.Email,
			}
			err = template.Template.ExecuteTemplate(w, template.Layout, data)
		} else {
			data := struct {
				Username        string
				FullName        string
				Address         string
				TelephoneNumber string
				Email           string
			}{
				Username: user.Username,
			}
			err = template.Template.ExecuteTemplate(w, template.Layout, data)
		}

		if err != nil {
			app.ServerError(w, err, "HandleSignIn : ExecuteTemplate")
			return
		}
	}
}

// HandleSignIn .
func (app *App) HandleSignIn(w http.ResponseWriter, r *http.Request) {
	template := app.View["login"]

	if r.Method == "POST" {
		user := &models.User{}
		err := r.ParseForm()
		if err != nil {
			app.ClientError(w, err, http.StatusBadRequest)
			return
		}

		if r.Header.Get("Content-Type") == "application/json" {
			w.Header().Set("Content-Type", "application-json")
			json.NewDecoder(r.Body).Decode(user)
		} else {
			user.Username = r.FormValue("username")
			user.Password = r.FormValue("password")
		}

		if _, err = mail.ParseAddress(user.Username); err != nil {
			if r.Header.Get("Content-Type") == "application/json" {
				json.NewEncoder(w).Encode(models.Response{
					StatusCode: http.StatusBadRequest,
					Message:    "username parameter has to be ..gmail.com",
					Error:      err.Error(),
				})
				return
			} else {
				err = template.Template.ExecuteTemplate(w, template.Layout, nil)
				if err != nil {
					app.ServerError(w, err, "HandleSignUp")
				}
				return
			}
		}

		id, err := app.Db.Authenticate(user.Username, user.Password)
		if err != nil {
			if err == models.ErrInvalidCredentials || err == models.ErrNoRecord {
				if r.Header.Get("Content-Type") == "application/json" {
					app.ClientError(w, err, http.StatusBadRequest)
					return
				} else {
					if err == models.ErrInvalidCredentials {
						errorMsg := struct {
							Err string
						}{
							Err: "incorrect password",
						}
						err = template.Template.ExecuteTemplate(w, template.Layout, errorMsg)
					} else {
						errorMsg := struct {
							Err string
						}{
							Err: "username does not exist",
						}
						err = template.Template.ExecuteTemplate(w, template.Layout, errorMsg)
					}
					if err != nil {
						app.ServerError(w, err, "HandleSignUp")
					}
					return
				}
			}
			app.ServerError(w, err, "HandleSignIn")
			return
		}

		app.LogInfo.Printf("%d is authenticated \n", id)
		app.Sessions.Put(r, "id", id)
		if r.Header.Get("Content-Type") == "application/json" {
			json.NewEncoder(w).Encode(models.Response{
				StatusCode: http.StatusOK,
				Message:    "user is authenticated",
				Error:      err.Error(),
				Data:       id,
			})
		} else {
			http.Redirect(w, r, "/mainprofile/", http.StatusSeeOther)
		}
	} else if r.Method == "GET" {
		err := template.Template.ExecuteTemplate(w, template.Layout, nil)
		if err != nil {
			app.ServerError(w, err, "HandleSignIn")
			return
		}
	}
}

// HandleSignUp .
// todo implement errors in forms
// *address alreay in use
// *password
func (app *App) HandleSignUp(w http.ResponseWriter, r *http.Request) {
	if r.Method == "GET" {
		template := app.View["signup"]
		err := template.Template.ExecuteTemplate(w, template.Layout, nil)
		if err != nil {
			app.ServerError(w, err, "HandleSignUp")
			return
		}
	} else if r.Method == "POST" {
		user := &models.User{}
		err := r.ParseForm()
		if err != nil {
			app.ClientError(w, err, http.StatusBadRequest)
			return
		}

		if r.Header.Get("Content-Type") == "application/json" {
			w.Header().Set("Content-Type", "application-json")
			json.NewDecoder(r.Body).Decode(user)
		} else {
			user.Username = r.FormValue("username")
			user.Password = r.FormValue("password")
		}

		if _, err = mail.ParseAddress(user.Username); err != nil {
			json.NewEncoder(w).Encode(models.Response{
				StatusCode: http.StatusBadRequest,
				Message:    "username parameter has to be ..gmail.com",
				Error:      err.Error(),
			})
			return
		}

		_, err = app.Db.GetUserByUsername(user.Username)
		if err != nil && err != models.ErrNoRecord {
			app.ServerError(w, err, "HandleSignUp : GetUser")
			return
		} else if err == models.ErrNoRecord {
			id, err := app.Db.AddUser(user)
			if err != nil {
				app.LogInfo.Println(err.(*mysql.MySQLError).Number)
				if err.(*mysql.MySQLError).Number == models.MySqlCodes.ALREADY_IN_USE {
					app.ClientError(w, models.ErrUserAlreadyInUse, http.StatusNotAcceptable)
					return
				}
				app.ServerError(w, err, "HandleSignUp : AddUser")
				return
			}

			if r.Header.Get("Content-Type") == "application/json" {
				json.NewEncoder(w).Encode(models.Response{
					StatusCode: http.StatusOK,
					Message:    "signedUp",
					Error:      "no error",
					Data:       id,
				})
			} else {

				template := app.View["login"]
				err = template.Template.ExecuteTemplate(w, template.Layout, nil)
				if err != nil {
					app.ServerError(w, err, "HandleSignUp")
					return
				}
			}
		} else {
			// if user exist with that name
			template := app.View["signup"]
			data := struct {
				Err string
			}{
				Err: "User with this email already exist",
			}
			err = template.Template.ExecuteTemplate(w, template.Layout, data)
			if err != nil {
				app.ServerError(w, err, "HandleSignUp")
				return
			}
		}

	}
}

// HandleSignOut .
func (app *App) HandleSignOut(w http.ResponseWriter, r *http.Request) {
	http.Redirect(w, r, "/", http.StatusSeeOther)
	app.Sessions.Destroy(r)
}

// HandleUserProfile .
func (app *App) HandleUserProfile(w http.ResponseWriter, r *http.Request) {
	id := app.Sessions.GetInt(r, "id")
	if r.Method == "GET" {
		template := app.View["profileInformation"]
		userProfile, err := app.Db.GetUserProfile(id)
		fmt.Println(userProfile)
		if err != nil && err != models.ErrNoRecord {
			app.ServerError(w, err, "GetUserProfile : HandleUserProfile")
			return
		}

		user, err := app.Db.GetUser(id)
		if err != nil && err != models.ErrNoRecord {
			app.ServerError(w, err, "GetUser : HandleUserProfile")
		}

		if userProfile != nil {
			data := struct {
				FullName        string
				Address         string
				TelephoneNumber string
				Email           string
				GoogleAuth      bool
			}{
				FullName:        userProfile.FullName,
				Address:         userProfile.Address,
				TelephoneNumber: userProfile.TelephoneNumber,
				Email:           userProfile.Email,
				GoogleAuth:      user.GoogleAuth,
			}

			app.LogInfo.Println(data)

			err = template.Template.ExecuteTemplate(w, template.Layout, data)
		} else {
			data := struct {
				FullName        string
				Address         string
				TelephoneNumber string
				Email           string
				GoogleAuth      bool
			}{
				Email:      user.Username,
				GoogleAuth: user.GoogleAuth,
			}
			app.LogInfo.Println(data)

			err = template.Template.ExecuteTemplate(w, template.Layout, data)
		}
		if err != nil {
			app.ServerError(w, err, "HandleUserProfile")
			return
		}
	} else if r.Method == "POST" {
		err := r.ParseForm()
		if err != nil {
			app.ClientError(w, err, http.StatusBadRequest)
			return
		}

		profile := &models.Profile{}
		if r.Header.Get("Content-Type") == "application/json" {
			w.Header().Set("Content-Type", "application-json")
			if err := json.NewDecoder(r.Body).Decode(profile); err != nil {
				app.ClientError(w, err, http.StatusBadRequest)
			}
		} else {
			profile.FullName = r.FormValue("fullname")
			profile.Address = r.FormValue("address")
			profile.TelephoneNumber = r.FormValue("telephoneNumber")
			profile.Email = r.FormValue("email")
		}
		user, err := app.Db.GetUser(id)
		if profile.Email == "" {
			if err != nil {
				app.ServerError(w, err, "HandleUserProfile")
				return
			}
			profile.Email = user.Username
		} else if profile.Email != user.Username {
			// we have to update email for user
			_, err := app.Db.Db.Exec(`UPDATE user set username = ? where username = ?`, profile.Email, user.Username)
			if err != nil {
				app.ServerError(w, err, "update email for user : GetUserProfile")
			}
			user.Username = profile.Email
		}

		_, err = app.Db.GetUserProfile(id)
		profile.Id = id
		fmt.Println(profile)
		template := app.View["mainProfile"]
		if err != nil && err == models.ErrNoRecord {
			// Put data to user
			err = app.Db.AddProfileToUser(profile)
			if err != nil {
				app.ServerError(w, err, "adding profile to user : GetUserProfile")
			}
		} else if err != nil {
			if err != nil {
				app.ServerError(w, err, "when getting user profile : GetUserProfile")
			}
		} else {
			// update user profile
			var query string = `UPDATE profile set fullname=?,email=?,address=?,telephoneNumber=? where id = ?`
			_, err := app.Db.Db.Exec(query, profile.FullName, profile.Email, profile.Address, profile.TelephoneNumber, id)
			if err != nil {
				app.ServerError(w, err, "Update profile : HandleUserProfile")
			}
		}
		data := struct {
			Username        string
			FullName        string
			Address         string
			TelephoneNumber string
			Email           string
		}{
			Username:        user.Username,
			FullName:        profile.FullName,
			Address:         profile.Address,
			TelephoneNumber: profile.TelephoneNumber,
			Email:           profile.Email,
		}

		if r.Header.Get("Content-Type") == "application/json" {
			json.NewEncoder(w).Encode(models.Response{
				StatusCode: http.StatusOK,
				Message:    "user is authenticated",
				Error:      err.Error(),
				Data:       data,
			})
			return
		}

		err = template.Template.ExecuteTemplate(w, template.Layout, data)
		if err != nil {
			app.ServerError(w, err, "HandleUserProfile")
			return
		}
	}
}

// HandleUpdatePassword .
func (app *App) HandleUpdatePassword(w http.ResponseWriter, r *http.Request) {
	w.Write([]byte("UpdatePassword"))
}

// HandleForgotPassword .
func (app *App) HandleForgotPassword(w http.ResponseWriter, r *http.Request) {
	template := app.View["forgotPassword"]
	err := template.Template.ExecuteTemplate(w, template.Layout, nil)
	if err != nil {
		app.ServerError(w, err, "NotFound")
		return
	}
}

// HandleResetLink .
func (app *App) HandleResetLink(w http.ResponseWriter, r *http.Request) {
	err := r.ParseForm()
	if err != nil {
		app.ClientError(w, err, http.StatusBadRequest)
	}
	username := r.FormValue("username")
	user, err := app.Db.GetUserByUsername(username)
	app.LogInfo.Println(user)
	switch err {
	case models.ErrNoRecord:
		template := app.View["forgotPassword"]
		errorMsg := struct {
			ErrUsername string
		}{
			ErrUsername: "no such email address registered",
		}
		err := template.Template.ExecuteTemplate(w, template.Layout, errorMsg)
		if err != nil {
			app.ServerError(w, err, "HandleResetLink")
			return
		}
		return
	case nil:
		// send reset link
	default:
		app.ServerError(w, err, "HandleResetLink")
		return
	}
	http.Redirect(w, r, "/", http.StatusSeeOther)
}

// HandleGoogleAuthentication.
var stateAuth string

func (app *App) HandleGoogleAuthentication(w http.ResponseWriter, r *http.Request) {
	stateAuth = helpers.RandString()
	url := app.GoogleOauthConfig.AuthCodeURL(stateAuth)
	http.Redirect(w, r, url, http.StatusTemporaryRedirect)
}

func (app *App) HandleGoogleCallback(w http.ResponseWriter, r *http.Request) {
	content, err := app.getUserInfo(r.FormValue("state"), r.FormValue("code"))
	if err != nil {
		app.ClientError(w, err, http.StatusBadRequest)
		http.Redirect(w, r, "/", http.StatusTemporaryRedirect)
		return
	}

	// save user and show main profile
	authcontent := &models.AuthContent{}

	err = json.Unmarshal(content, authcontent)
	if err != nil {
		app.ServerError(w, err, "HandleGoogleCallback")
		return
	}

	var id int
	if user, err := app.Db.GetUserByUsername(authcontent.Email); err != nil && err != models.ErrNoRecord {
		app.ClientError(w, err, http.StatusInternalServerError)
	} else if err == models.ErrNoRecord {
		user = &models.User{
			Username:   authcontent.Email,
			Password:   authcontent.ID,
			GoogleAuth: true,
		}
		app.LogInfo.Println("New user added")
		id, err = app.Db.AddUser(user)
		if err != nil && err != models.ErrUserAlreadyInUse {
			app.ServerError(w, err, "HandleGoogleCallBack : AddUser")
			return
		}

		profile := &models.Profile{}
		profile.Email = user.Username
		profile.Id = id
		app.LogInfo.Println(profile)
		err := app.Db.AddProfileToUser(profile)
		if err != nil {
			app.ServerError(w, err, "HandleGoogleCallback : AddProfileToUser")
			return
		}

		app.Sessions.Put(r, "id", id)
	} else {
		app.LogInfo.Println("user Found as: ", user)
		app.Sessions.Put(r, "id", user.Id)
	}

	fmt.Println(app.Sessions.GetInt(r, "id"))
	// fmt.Fprintf(w, "Content: %s\n", content)
	http.Redirect(w, r, "/mainprofile/", http.StatusTemporaryRedirect)
}

func (app *App) getUserInfo(state string, code string) ([]byte, error) {
	if state != stateAuth {
		return nil, fmt.Errorf("invalid oauth state")
	}

	token, err := app.GoogleOauthConfig.Exchange(oauth2.NoContext, code)
	if err != nil {
		return nil, fmt.Errorf("code exchange failed: %s", err.Error())
	}

	response, err := http.Get("https://www.googleapis.com/oauth2/v2/userinfo?access_token=" + token.AccessToken)
	if err != nil {
		return nil, fmt.Errorf("failed getting user info: %s", err.Error())
	}

	defer response.Body.Close()
	contents, err := ioutil.ReadAll(response.Body)
	if err != nil {
		return nil, fmt.Errorf("failed reading response body: %s", err.Error())
	}

	return contents, nil
}

// NotFound its custom notfound handler
func (app *App) NotFound(w http.ResponseWriter, r *http.Request) {
	template := app.View["404"]
	err := template.Template.ExecuteTemplate(w, template.Layout, nil)
	if err != nil {
		app.ServerError(w, err, "NotFound")
		return
	}
}
