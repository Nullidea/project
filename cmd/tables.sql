CREATE TABLE user (
	id int NOT NULL AUTO_INCREMENT,
	username varchar(255) not null,
	password varchar(255),
	googleauth boolean default false,
	PRIMARY KEY (id)
);

CREATE TABLE profile(
	id int not null,
	fullName        varchar(255),
	email           varchar(255),
	address         varchar(255),
	telephoneNumber varchar(255),
	foreign key(id) references user(id)
);