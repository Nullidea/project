package cmd

import (
	"net/http"

	"github.com/gorilla/mux"
)

// RegisterRouters MVP
func RegisterRouters(app *App) *mux.Router {
	mux := mux.NewRouter()
	mux.NotFoundHandler = http.HandlerFunc(app.NotFound)

	mux.Handle("/mainprofile/", app.EnableSessAuth(app.HandleMainPage)).Methods("GET") // need auth
	mux.Handle("/", app.EnableSess(app.HandleSignIn)).Methods("POST", "GET")
	mux.Handle("/signout/", app.EnableSessAuth(app.HandleSignOut)).Methods("GET") // need auth
	mux.Handle("/signup/", app.EnableSess(app.HandleSignUp)).Methods("POST", "GET")
	mux.Handle("/profileInformation/", app.EnableSessAuth(app.HandleUserProfile)).Methods("GET", "POST") // need auth
	mux.Handle("/updatepassword/", app.EnableSessAuth(app.HandleUpdatePassword)).Methods("PUT")          // need auth
	mux.Handle("/forgotpassword/", app.EnableSess(app.HandleForgotPassword)).Methods("GET")              // need auth
	mux.Handle("/sendresetlink/", app.EnableSess(app.HandleResetLink)).Methods("GET")
	mux.Handle("/loginGoogle/", app.EnableSess(app.HandleGoogleAuthentication)).Methods("GET")
	mux.Handle("/googleCallback/", app.EnableSess(app.HandleGoogleCallback)).Methods("GET")
	mux.Use(app.recover, app.logRequest, app.middleware)
	return mux
}
