package cmd

import (
	"log"
	"project/pkg/models/db"
	"project/view"
	"time"

	"github.com/golangcollege/sessions"
	"golang.org/x/oauth2"
)

// App structure is the core struct of server
type App struct {
	LogError          *log.Logger
	LogInfo           *log.Logger
	Db                *db.AppDb
	View              map[string]*view.View
	Sessions          *sessions.Session // sessions
	GoogleOauthConfig *oauth2.Config
}

// SetSessions .
func (a *App) SetSessions(key []byte) {
	a.Sessions = sessions.New(key)
	a.Sessions.Lifetime = 12 * time.Hour
}
