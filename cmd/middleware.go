package cmd

import (
	"fmt"
	"net/http"
	"project/pkg/models"
)

// middleware .
func (app *App) middleware(next http.Handler) http.Handler {
	m := func(w http.ResponseWriter, r *http.Request) {
		fmt.Println("Middleware")
		w.Header().Set("X-XSS-Protection", "1; mode=block")
		w.Header().Set("X-Frame-Options", "deny")
		next.ServeHTTP(w, r)
	}

	return http.HandlerFunc(m)
}

func (app *App) logRequest(next http.Handler) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		app.LogInfo.Printf("%s - %s %s %s\n", r.RemoteAddr, r.Proto, r.Method, r.URL)
		next.ServeHTTP(w, r)
	})
}

// Recover .
func (app *App) recover(next http.Handler) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		defer func() {
			if err := recover(); err != nil {
				w.Header().Set("Connection", "close")
				app.ServerError(w, fmt.Errorf("Panic : %s", err), "recover")
			}
		}()
		next.ServeHTTP(w, r)
	})
}

//EnableSess .
func (app *App) EnableSess(f func(http.ResponseWriter, *http.Request)) http.Handler {
	return app.Sessions.Enable(http.HandlerFunc(f))
}

//EnableSessAuth .
func (app *App) EnableSessAuth(f func(http.ResponseWriter, *http.Request)) http.Handler {
	return app.Sessions.Enable(app.Auth(http.HandlerFunc(f)))
}

// Auth .
func (app *App) Auth(next http.Handler) http.Handler {

	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		id := app.Sessions.GetInt(r, "id")
		fmt.Println(id)
		if id == 0 {
			app.ClientError(w, models.ErrUserNotAuthorized, http.StatusForbidden)
			return
		}
		next.ServeHTTP(w, r)
	})
}
