package view

import (
	"html/template"
	"path/filepath"
)

var path string = "view/html/layouts/*.gohtml"

// View .
type View struct {
	Template *template.Template
	Layout   string
}

// NewView .
func NewView(layout string, files ...string) *View {
	filesIndirer, err := filepath.Glob(path)
	if err != nil {
		panic(err)
	}
	files = append(
		files,
		filesIndirer...,
	)
	temp, err := template.ParseFiles(files...) // files... makes it strings

	if err != nil {
		panic(err)
	}

	return &View{
		Template: temp,
		Layout:   layout,
	}
}

// GetView .
func GetView() map[string]*View {
	v := map[string]*View{
		"mainProfile":        NewView("bootstrap", "view/html/home.gohtml"),
		"404":                NewView("bootstrap", "view/html/404page.gohtml"),
		"login":              NewView("bootstrap", "view/html/login.gohtml"),
		"signup":             NewView("bootstrap", "view/html/signup.gohtml"),
		"profileInformation": NewView("bootstrap", "view/html/profileInformation.gohtml"),
		"forgotPassword":     NewView("bootstrap", "view/html/forgotPassword.gohtml"),
		"resetPassword":      NewView("bootstrap", "view/html/resetPassword.gohtml"),
	}

	return v
}
