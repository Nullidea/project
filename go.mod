module project

go 1.15

require (
	github.com/go-sql-driver/mysql v1.6.0
	github.com/golangcollege/sessions v1.2.0
	github.com/gorilla/mux v1.8.0
	github.com/joho/godotenv v1.3.0
	golang.org/x/crypto v0.0.0-20200622213623-75b288015ac9
	golang.org/x/oauth2 v0.0.0-20211104180415-d3ed0bb246c8
	golang.org/x/sys v0.0.0-20210927094055-39ccf1dd6fa6 // indirect
)
