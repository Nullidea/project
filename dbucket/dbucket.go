package dbucket

import (
	"log"
	"os"

	"github.com/joho/godotenv"
)

// Load ..
func Load() {
	err := godotenv.Load(".env")
	if err != nil {
		log.Fatalf("Error loading .env file")
		os.Exit(1)
	}
}

//GetEnv .
func GetEnv(key string) string {
	return os.Getenv(key)
}
