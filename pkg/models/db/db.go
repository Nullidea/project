package db

import (
	"database/sql"
)

// AppDb .
type AppDb struct {
	Db *sql.DB
}

// Connect handles the connection to database
func Connect(info string) (*sql.DB, error) {
	db, err := sql.Open("mysql", info)
	if err != nil {
		return nil, err
	}
	if err = db.Ping(); err != nil {
		return nil, err
	}
	return db, nil
}

// Set is setter for connection.
// Sets the given database to connect
func Set(database *sql.DB) *AppDb {
	return &AppDb{
		Db: database,
	}
}

// Get is getter for AppDb.
// Gets the database connection
func Get(db *AppDb) *sql.DB {
	return db.Db
}
