package db

import (
	"database/sql"
	"project/pkg/models"

	"golang.org/x/crypto/bcrypt"
)

// AddUser .
func (db *AppDb) AddUser(user *models.User) (int, error) {
	var id int64
	hashedpass, err := bcrypt.GenerateFromPassword([]byte(user.Password), 12)
	if err != nil {
		return 0, err
	}

	// psql uses VALUES(?, ?, ?)
	// mysql uses VALUES($1, $2, $3)
	// oracle uses VALUES(:val1, :val2, :val3)
	query := `INSERT INTO user (username, password, googleauth)
	VALUES(?,?,?);`

	row, err := db.Db.Exec(query, user.Username, hashedpass, user.GoogleAuth)
	if err != nil && err != sql.ErrNoRows {
		return 0, err
	}

	id, err = row.LastInsertId()
	if err != nil {
		return 0, err
	}

	return int(id), nil
}

func (db *AppDb) AddProfileToUser(user *models.Profile) error {
	query := `INSERT INTO profile (id, fullname, email, address, telephoneNumber) VALUES((SELECT id from user where id = ?),?,?,?,?)`
	_, err := db.Db.Exec(query, user.Id, user.FullName, user.Email, user.Address, user.TelephoneNumber)
	if err != nil && err != sql.ErrNoRows {
		return err
	}

	return nil
}

// GetUserProfile .
func (db *AppDb) GetUserProfile(id int) (*models.Profile, error) {
	profile := &models.Profile{}
	query := `select fullname,email,address,telephoneNumber from profile where id = ?`
	err := db.Db.QueryRow(query, id).Scan(&profile.FullName, &profile.Email, &profile.Address, &profile.TelephoneNumber)
	if err == sql.ErrNoRows {
		return nil, models.ErrNoRecord
	} else if err != nil {
		return nil, err
	}

	profile.Id = id
	return profile, nil
}

// GetUser .
func (db *AppDb) GetUser(id int) (*models.User, error) {
	user := &models.User{}
	query := `select username,googleauth from user where id = ?`
	err := db.Db.QueryRow(query, id).Scan(&user.Username, &user.GoogleAuth)
	if err == sql.ErrNoRows {
		return nil, models.ErrNoRecord
	} else if err != nil {
		return nil, err
	}

	user.Id = id

	return user, nil
}

// GetUser .
func (db *AppDb) GetUserByUsername(username string) (*models.User, error) {
	user := &models.User{}
	var id int
	query := `select id from user where username = ?`
	err := db.Db.QueryRow(query, username).Scan(&id)
	if err == sql.ErrNoRows {
		return nil, models.ErrNoRecord
	} else if err != nil {
		return nil, err
	}

	user.Id = id
	user.Username = username

	return user, nil
}

// Authenticate checks the user password.
// If user authenticated returns id and nil,
// otherwise returns 0 and err
func (db *AppDb) Authenticate(email, password string) (int, error) {
	var id int
	var hashedPassword []byte

	query := `select id, password from user where username = ?`
	err := db.Db.QueryRow(query, email).Scan(&id, &hashedPassword)
	if err != nil {
		if err == sql.ErrNoRows {
			return 0, models.ErrNoRecord
		}
		return 0, err
	}
	err = bcrypt.CompareHashAndPassword(hashedPassword, []byte(password))
	if err == bcrypt.ErrMismatchedHashAndPassword {
		return 0, models.ErrInvalidCredentials
	} else if err != nil {
		return 0, err
	}
	return id, nil
}

// UpdateUser .
func (db *AppDb) UpdateUser() error {
	return nil
}
