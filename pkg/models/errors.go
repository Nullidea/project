package models

import "errors"

var MySqlCodes = struct {
	ALREADY_IN_USE uint16
}{
	ALREADY_IN_USE: 1061,
}

// ErrUserNotAuthorized .
var ErrUserNotAuthorized = errors.New("user has to be authorized")

// ErrNoRecord .
var ErrNoRecord = errors.New("no data found")

// ErrInvalidCredentials .
var ErrInvalidCredentials = errors.New("invalid credentials")

// ErrUserAlreadyInUse .
var ErrUserAlreadyInUse = errors.New("user already in use")
