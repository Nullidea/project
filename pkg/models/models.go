package models

// User .
type User struct {
	Id         int
	Username   string `json:"username" db:"username"`
	Password   string `json:"password" db:"password"`
	GoogleAuth bool   `db:"googleauth"`
}

// Profile .
type Profile struct {
	Id              int
	FullName        string `json:"fullname" db:"fullname"`
	Email           string `json:"email" db:"email"`
	Address         string `json:"address" db:"address"`
	TelephoneNumber string `json:"telephoneNumber" db:"telephoneNumber"`
}

// Response .
type Response struct {
	StatusCode int         `json:"statusCode"`
	Message    string      `json:"message"`
	Error      string      `json:"error"`
	Data       interface{} `json:"data"`
}

// Google resp
type AuthContent struct {
	ID            string `json:"id"`
	Email         string `json:"email"`
	VerifiedEmail bool   `json:"verified_email"`
	Picture       string `json:"picture"`
}
