package main

import (
	"flag"
	"fmt"
	"log"
	"net/http"
	"os"

	"project/cmd"
	"project/dbucket"
	"project/pkg/models/db"
	"project/view"

	_ "github.com/go-sql-driver/mysql"
	"golang.org/x/oauth2"
	"golang.org/x/oauth2/google"
)

// bfec3d0a4f47f3
// 234176b9
// us-cdbr-east-05.cleardb.net

func main() {
	dbucket.Load()
	port := flag.String("addr", dbucket.GetEnv("PORT"), "server port")
	flag.Parse()

	// dbInfo := fmt.Sprintf("%s:%s@tcp(127.0.0.1:3306)/%s", dbucket.GetEnv("USER"), dbucket.GetEnv("PASSWORD"), dbucket.GetEnv("DBNAME"))
	dbInfo := fmt.Sprintf("%s:%s@tcp(%s:3306)/%s", dbucket.GetEnv("USER"), dbucket.GetEnv("PASSWORD"), dbucket.GetEnv("URL"), dbucket.GetEnv("DBNAME"))

	dataBase, err := db.Connect(dbInfo)

	if err != nil {
		log.Fatal(err)
	}

	defer dataBase.Close()

	views := view.GetView()

	app := &cmd.App{
		LogError: log.New(os.Stderr, "ERORR\t\t\t", log.Lshortfile),
		LogInfo:  log.New(os.Stdout, "INFO\t\t\t", log.Ltime),
		Db:       db.Set(dataBase),
		View:     views,
		GoogleOauthConfig: &oauth2.Config{
			RedirectURL:  "https://" + os.Getenv("HEROKU_APP_NAME") + dbucket.GetEnv("REDIRECT_URL"),
			ClientID:     dbucket.GetEnv("GOOGLE_CLIENT_ID"),
			ClientSecret: dbucket.GetEnv("GOOGLE_CLIENT_SECRET"),
			Scopes:       []string{"https://www.googleapis.com/auth/userinfo.email"},
			Endpoint:     google.Endpoint,
		},
	}

	app.LogInfo.Printf("Datatbase %s is connected", dbucket.GetEnv("DBNAME"))
	app.SetSessions([]byte(os.Getenv("KEY")))

	handler := cmd.RegisterRouters(app)

	server := &http.Server{
		Addr:     *port,
		Handler:  handler,
		ErrorLog: app.LogError,
	}

	app.LogInfo.Printf("Server is starting on http://localhost%s", *port)
	app.LogError.Fatal(server.ListenAndServe())
}
